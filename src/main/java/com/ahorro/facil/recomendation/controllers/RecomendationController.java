package com.ahorro.facil.recomendation.controllers;

import java.util.List;

import com.ahorro.facil.recomendation.model.Recomendation;
import com.ahorro.facil.recomendation.repositories.RecomendationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/recomendation")

public class RecomendationController {

    @Autowired
    RecomendationRepository recomendationRepository;


    @GetMapping("/recomendation")
    public ResponseEntity<List<Recomendation>> findRecomendationByUserId(@RequestParam(required = true) long id) {
        List<Recomendation> recomendation = recomendationRepository.findByUserId(id);
        return new ResponseEntity<>(recomendation, HttpStatus.OK);
    }

    @PostMapping("/recomendation")
    public ResponseEntity<Recomendation> registerRecomendation(@RequestBody Recomendation recomendation) {
        try {
            Recomendation created = recomendationRepository.save(
                    new Recomendation(recomendation.getTitle(), recomendation.getAmount(), recomendation.getUserId(), recomendation.isActive()));
            return new ResponseEntity<>(created, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
