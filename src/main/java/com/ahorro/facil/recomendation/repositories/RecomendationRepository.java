package com.ahorro.facil.recomendation.repositories;
import com.ahorro.facil.recomendation.model.Recomendation;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface RecomendationRepository extends JpaRepository<Recomendation, Long> {

    @Query(
        value = "SELECT * FROM recomendation i WHERE i.user_id = ?1",
        nativeQuery = true
    )
    List<Recomendation> findByUserId(long userId);    

}
